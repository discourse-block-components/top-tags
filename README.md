# Block: Top Tags

A block component that can be used within a layout framework like Blocks or Bars. Lists the most popular tags.

## name:

`block-top-tags`

## params

| name  | description                             | default  | value  |
| ----- | --------------------------------------- | -------- | ------ |
| title | block title                             | Top Tags | string |
| count | limits number of results                | 5        | number |
| class | additional css class name for the block |          | string |

## template

```scss
.block-top-tags {
  &__container {
  }
  &__title {
  }
  &__list {
  }
}
```
