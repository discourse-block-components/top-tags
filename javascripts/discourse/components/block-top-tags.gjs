import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { ajax } from "discourse/lib/ajax";

export default class BlockTopTags extends Component {
  @tracked topTags;

  <template>
    <div class="block-top-tags {{this.blockClass}}">
      <div class="block-top-tags__container">
        <h2 clas="block-top-tags__title">{{this.blockTitle}}</h2>
        <ul class="block-top-tags__list">
          {{#each this.topTags as |tag|}}
            <li>
              <a href="/tag/{{tag.id}}">{{tag.id}}</a></li>
          {{/each}}
        </ul>
      </div>
    </div>
  </template>

  constructor() {
    super(...arguments);
    this.blockTitle = this.args?.params?.title || "Top Tags";
    this.blockClass = this.args?.params?.class;

    this.getTags();
  }

  @action
  async getTags() {
    const count = this.args?.params?.count || 5;

    const tagsList = await ajax(`/tags.json`);
    this.topTags = tagsList.tags.slice(0, count);
  }
}
